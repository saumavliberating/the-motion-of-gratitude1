﻿using Microsoft.EntityFrameworkCore;
using MotionOfGratitude.entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotionOfGratitude.database
{
   public class MotionofGratitude : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Reseller> Resellers { get; set; }
        public DbSet<Commission> Commissions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\\mssqllocaldb;Database=aspnet-MotionOfGratitude.web-D7A45961-910C-48A9-9A1B-6BE5337B2E0C;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

    }

}


