﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotionOfGratitude.entites
{
    public class Commission
    {
        public int ID { get; set; }
        public string CommissionTo { get; set; }
        public string AccountNo { get; set; }
        public decimal Amount { get; set; }
        
    }
}
